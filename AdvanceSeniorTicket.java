import java.time.LocalDate;
/**
 * Project County Movie Theater
 * Author Nick Tagliamonte
 * Created 2/2/23
 * Description an AdvanceSeniorTicket is a type of ticket which requires an ID check to construct, in the form of a boolean variable passed into the constructor
 * It extends AdvanceTicket and has all of the methods and fields available to an advanced ticket, but at a lower price
 */
public class AdvanceSeniorTicket extends AdvanceTicket {
	public boolean IDStatus;
	public AdvanceSeniorTicket(int ticketNumber, LocalDate date, boolean IDStatus) {
		super(ticketNumber, date);
		this.IDStatus = IDStatus;
		this.price = 16.0;
	}
}