import java.time.LocalDate;
/**
 * Project County Movie Theater
 * Author Nick Tagliamonte
 * Created 2/2/23
 * Description: The ticket class is a superclass for WalkupTicket, AdvanceTicket, and AdvanceSeniorTicket.  It is never instantiated on its own.
 * It contains fields for a TicketNumber, which is used to construct the object, as well as a show name, date, and price.  There are setters and getters for each.
 * At the end is a toString method which outputs the data in the fields in String form
 */
public class Ticket {
	public int ticketNumber;
	public String show;
	public LocalDate dateOfShow;
	public double price;
	
	public Ticket(int ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public int getTicketNumber() {
		return ticketNumber;
	}

	public void setTicketNumber(int ticketNumber) {
		this.ticketNumber = ticketNumber;
	}

	public String getShow() {
		return show;
	}

	public void setShow(String show) {
		this.show = show;
	}

	public LocalDate getDateOfShow() {
		return dateOfShow;
	}

	public void setDateOfShow(LocalDate dateOfShow) {
		this.dateOfShow = dateOfShow;
	}

	public double getPrice() {
		return price;
	}

	public void setPrice(double price) {
		this.price = price;
	}

	public String toString() {
		return String.format("Ticket Number " + ticketNumber + " for show " + show + " on " + dateOfShow + ".\nPrice: $'%f.2'", price);
	}	
}