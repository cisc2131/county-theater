import java.time.LocalDate;
/**
 * Project County Movie Theater
 * Author Nick Tagliamonte
 * Created 2/2/23
 * Description: AdvanceTicket is a subclass of Ticket and has access to all of Tickets methods.  On construction, this type of ticket requires a date of
 * the show to be passed in rather than immediately assigning today's date.
 * The price is set at 18.50 in the constructor.
 * There is also a field for the payment method, and a getter and setter for this field
 */
public class AdvanceTicket extends Ticket {
	public String paymentMethod;
	public AdvanceTicket(int ticketNumber, LocalDate date) {
		super(ticketNumber);
		this.price = 18.5;
		this.dateOfShow = date;
	}
	public String getPaymentMethod() {
		return paymentMethod;
	}
	public void setPaymentMethod(String paymentMethod) {
		this.paymentMethod = paymentMethod;
	}
}