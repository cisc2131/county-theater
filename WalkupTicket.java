/**
 * Project County Movie Theater
 * Author Nick Tagliamonte
 * Created 2/2/23
 * Description: WalkupTicket is a subclass of Ticket.  It is constructed by the TicketNumber.  Because a walkup ticket is always sold same-day, the date field is automatically set to todays date using a LocalDate object.
 * The price is set to 22.50 in the constructor.
 */
public class WalkupTicket extends Ticket {
	public WalkupTicket(int ticketNumber) {
		super(ticketNumber);
		this.dateOfShow = java.time.LocalDate.now();
		this.price = 22.5;
	}	
}