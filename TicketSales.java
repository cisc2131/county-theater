import java.awt.BorderLayout;
import java.awt.FlowLayout;
import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JTextField;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JComboBox;
import javax.swing.Box;
import javax.swing.ButtonGroup;
import javax.swing.DefaultComboBoxModel;
import java.time.LocalDate;
import java.util.ArrayList;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.awt.Component;
/**
 * Project: County Theater
 * Date: 2/5/23
 * @author Nick Tagliamonte
 * Description: The TicketSales class contains the main method for this assignment.  When run, a window is launched by main calling the TicketSales constructor. The window contains a button to generate a walkup ticket or an advance ticket.
 * There is also an option to get the total sales figures for the day.
 * A running tally of the number of tickets sold is kept on this screen.
 * When one of the two ticket buttons is pressed, an action listener opens a new dialog which prompts for the information needed to generate a ticket.
 * The ticket is then added to an ArrayList of tickets sold for a day which will restart whenever the program is launched (i.e. once per day)
 * When the getSales button is pressed, a dialog with the day's sales totals is opened
 */
public class TicketSales extends JDialog implements FileIO{
	
	//These are global variables to allow them to be accessed by multiple methods without passing
	private final ArrayList<Ticket> numberSold = new ArrayList<Ticket>();
	private double totalSales = 0;
	private int currentTicketNumber = 1;
	public JLabel ticketCount;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		try {
			TicketSales dialog = new TicketSales();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public TicketSales() {
		setBounds(100, 100, 350, 100);
		getContentPane().setLayout(new BorderLayout());
		JPanel contentPanel = new JPanel();
		contentPanel.setLayout(new FlowLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		
		Box verticalBox = Box.createVerticalBox();
		contentPanel.add(verticalBox);
		
		Box horizontalBox = Box.createHorizontalBox();
		verticalBox.add(horizontalBox);
		
		JButton walkupButton = new JButton("Walkup Ticket");
		walkupButton.addMouseListener(new MouseAdapter() {
			/**
			 * This is the actionListener to call the generateWalkup method when the walkup button is clicked
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				generateWalkupTicket();
			}
		});		
		horizontalBox.add(walkupButton);
		
		JButton advancedButton = new JButton("Advanced Ticket");
		advancedButton.addMouseListener(new MouseAdapter() {
			/**
			 * This is the actionListener to call the generateAdvanceTicket method when the advance button is clicked
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				generateAdvanceTicket();
			}
		});
		horizontalBox.add(advancedButton);
		
		JButton getSalesButton = new JButton("Get Sales");
		getSalesButton.addMouseListener(new MouseAdapter() {
			/**
			 * This is the actionListener to call the getSales method when the get sales button is clicked
			 */
			@Override
			public void mouseClicked(MouseEvent e) {
				getSales();
			}
		});
		horizontalBox.add(getSalesButton);
		
		ticketCount = new JLabel("Tickets Sold Today: " + (currentTicketNumber - 1));
		ticketCount.setAlignmentX(Component.CENTER_ALIGNMENT);
		verticalBox.add(ticketCount);		
	}
	
	/**
	 * Show the days sales in a new dialog box
	 */
	public void getSales() {
		JOptionPane.showMessageDialog(null, String.format("The total sales for the day are $%,.2f", totalSales));
	}

	/**
	 * Opens a new dialog which prompts for a show name, creates a new WalkupTicket with a ticket number and show name, adds the ticket to the ArrayList, 
	 * adds the sale price to the total sales, increments the current ticket number by one, 
	 * and writes the ticket info to a file by calling FileIO's writeTicket method
	 */
	public void generateWalkupTicket() {
		String showName = JOptionPane.showInputDialog("Enter the name of the show: ");
		Ticket newWalkup = new WalkupTicket(currentTicketNumber);
		newWalkup.setShow(showName);
		numberSold.add(newWalkup);
		totalSales += newWalkup.getPrice();
		currentTicketNumber++;
		ticketCount.setText("Tickets Sold Today: " + (currentTicketNumber - 1));
		try {
			FileIO.writeTicket(newWalkup);
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	/**
	 * opens a new dialog which prompts for the information needed to create an advance ticket. 
	 * The senior ID check occurs in this window, and depending on the result of the check an AdvanceTicket or an AdvanceSeniorTicket will be created
	 */
	public void generateAdvanceTicket() {
		String showName;
		CharSequence date;
		String paymentType;
		Boolean isSenior;
		
		//This code block generates the dialog contents
		JPanel advancedPanel = new JPanel();		
		Box verticalBox = Box.createVerticalBox();
		advancedPanel.add(verticalBox);		
		Box showBox = Box.createHorizontalBox();
		verticalBox.add(showBox);		
		Box dateBox = Box.createHorizontalBox();
		verticalBox.add(dateBox);		
		Box seniorBox = Box.createHorizontalBox();
		verticalBox.add(seniorBox);		
		Box paymentBox = Box.createHorizontalBox();
		verticalBox.add(paymentBox);		
		JTextField showNameField = new JTextField(20);
		showBox.add(new JLabel("Show Name: "));
		showBox.add(showNameField);		
		JTextField dateField = new JTextField(10);		
		dateBox.add(new JLabel("Enter the date in the form yyyy-mm-dd: "));
		dateBox.add(dateField);		
		JLabel seniorCheck = new JLabel("Check whether this is a senior ticket");	
		JRadioButton seniorButton = new JRadioButton("Senior");	
		JRadioButton notSeniorButton = new JRadioButton("Not Senior");
		ButtonGroup group = new ButtonGroup();
		group.add(seniorButton);
		group.add(notSeniorButton);
		seniorBox.add(seniorCheck);
		seniorBox.add(seniorButton);
		seniorBox.add(notSeniorButton);		
		JComboBox comboBoxPayment = new JComboBox();
		comboBoxPayment.setModel(new DefaultComboBoxModel(new String[] {"Cash", "Credit", "Check"}));
		paymentBox.add(new JLabel("Select Payment Type: "));
		paymentBox.add(comboBoxPayment);	
		
		//This variable declaration and if/else statement assign the input collected to variabled so that they can be passed to a constructor
		int getShow = JOptionPane.showConfirmDialog(null,  advancedPanel, "Enter the Show Details", JOptionPane.OK_CANCEL_OPTION);
		if (getShow == JOptionPane.OK_OPTION) {
			showName = showNameField.getText();
			date = dateField.getText();
			paymentType = comboBoxPayment.getSelectedItem().toString();
			isSenior = seniorButton.isSelected();
		}
		else {
			showName = null;
			date = null;
			paymentType = null;
			isSenior = false;
			System.exit(DISPOSE_ON_CLOSE);
		}
		
		//If the SeniorID shows that this should be a senior ticket, one is created here
		if (isSenior) {
			//create the AdvanceSeniorTicket
			AdvanceTicket newAdvancedSeniorTicket = new AdvanceSeniorTicket(currentTicketNumber, LocalDate.parse(date), true);
			//set the showName and paymentType
			newAdvancedSeniorTicket.setShow(showName);
			newAdvancedSeniorTicket.setPaymentMethod(paymentType);
			//add this ticket to the ArrayList of tickets
			numberSold.add(newAdvancedSeniorTicket);
			//add the price of this ticket to the totalSales
			totalSales += newAdvancedSeniorTicket.getPrice();
			//increment the number of tickets sold
			currentTicketNumber++;
			//increment the ticketCount label in the main application window 
			ticketCount.setText("Tickets Sold Today: " + (currentTicketNumber - 1));
			//call the FileIO interface's writeTicket method to write the ticket information to a log file
			try {
				FileIO.writeTicket(newAdvancedSeniorTicket);
			} catch (IOException e) {
				e.printStackTrace();
			}
		//This else block performs the same actions as above but for an AdvanceTicket rather than an advanceSeniorTicket
		} else {
			AdvanceTicket newAdvancedTicket = new AdvanceTicket(currentTicketNumber, LocalDate.parse(date));
			newAdvancedTicket.setShow(showName);
			newAdvancedTicket.setPaymentMethod(paymentType);
			numberSold.add(newAdvancedTicket);
			totalSales += newAdvancedTicket.getPrice();
			currentTicketNumber++;
			ticketCount.setText("Tickets Sold Today: " + (currentTicketNumber - 1));
			try {
				FileIO.writeTicket(newAdvancedTicket);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}			
	}	
}