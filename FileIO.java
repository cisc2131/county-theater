/**
 * Project County Theater
 * Author Nick Tagliamonte
 * Created 2/5/23
 * Description: The FileIO interface contains a method to write information from a ticket to a file in csv format.
 * If the file does not exist, it will be created
 * The file is not overwritten to maintain a log of all past sales
 * In practice, this function may be improved by generating a new file for each new day's tickets and moving the previous day's file to a log archive
 */

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public interface FileIO {
	
	public static void writeTicket(Ticket ticket) throws IOException {
		File file = new File("ticketsSold");
		if(file.exists() == false) {
			file.createNewFile();
		}
		BufferedWriter writer = new BufferedWriter(new FileWriter(file, true));
		writer.write(ticket.getShow() + "," + ticket.getDateOfShow() + "," + ticket.getPrice() + "\n");
		writer.close();
		
	}
}